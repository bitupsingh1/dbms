import React from "react";
import HeaderCSS from "../components/Header.module.css";
export default function Header() {
  return (
    <div className={HeaderCSS.header}>
      <ul className={HeaderCSS.headerlist}>
        <li className="logo">LOGO</li>
        <li>PROFILE</li>
        <li>BOOKS</li>
        <li>Log In</li>
        <li>NOTIFICATIONS</li>
      </ul>
    </div>
  );
}
